using JsonRPC;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace api.Controllers
{
    //dotnet publish -c release
    [ApiController]
    // [Route("api/[controller]")]
    [Route("api/[controller]")]
    public class ElectrumController : Base
    {
        private readonly IElectrum electrum;

        public ElectrumController(IElectrum electrum)
        {
            this.electrum = electrum;
        }
        [HttpGet]
        public IActionResult IsOk()
        {
            return Ok("Its Ok!");
        }

        //https://localhost:5003/api/Electrum/GetAddress 
        [HttpGet("GetAddress")]
        public IActionResult GetAddress()
        {
            return this.ResponceHandler(electrum.GetAddress());
        } 

        //https://localhost:5003/api/Electrum/GetExAddresses 
        [HttpGet("GetExAddresses")]
        public IActionResult GetExAddresses()
        {
            return this.ResponceHandler(electrum.GetExpandedAddresses());
        }

        //https://localhost:5003/api/Electrum/GetAddresses
        [HttpGet("GetAddresses")]
        public IActionResult GetAddresses()
        {
            return this.ResponceHandler(electrum.GetAddresses());
        }

        //https://localhost:5003/api/Electrum/CreateTransaction
        // [HttpPost("CreateTransaction")]
        // public IActionResult CreateTransaction(models.TransactionDTO transaction)
        // {
        //     return this.ResponceHandler(electrum.CreateAndSignTransaction(transaction));
        // } 
 

        //https://localhost:5003/api/Electrum/CreateUnsignedTransaction
        [HttpPost("CreateUnsignedTransaction")]
        public IActionResult CreateUnsignedTransaction(models.TransactionDTO transaction)
        {
            return this.ResponceHandler(electrum.CreateUnsignedTransaction(transaction));
        }

        //https://localhost:5003/api/Electrum/SignTransaction
        [HttpPost("SignTransaction")]
        public IActionResult SignTransaction(UnsignedTx uTransaction)
        {
            return this.ResponceHandler(electrum.SignTransaction(uTransaction));
        }
    }
}
//https://localhost:5003/swagger/index.html