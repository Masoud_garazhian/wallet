﻿using System.Runtime.Serialization;

namespace JsonRPC.Response.Model
{
    [DataContract]
    public class SimpleBoolResponseClass : AbstractResponseClass
    {
        [DataMember]
        public bool? result { get; set; }
    }
}
