﻿

// Electrum-3.3.8


using System;

namespace JsonRPC.Request.Methods.Core
{
    /// <summary>
    /// Получить мастер-ключ. Возврат главного открытого ключа Кошелька
    /// ~ ~ ~
    /// Get master public key. Return your wallet's master public key
    /// </summary>
    class GetMpkMethodClass : AbstractMethodClass
    {
        public override string method => "getmpk";
        public GetMpkMethodClass(Electrum_JSONRPC_Client client) : base(client)
        {

        }

        public override object execute()
        {
            return Client.Execute(method, options);
            // return deserialized object //Masoud
        }
    }
}
