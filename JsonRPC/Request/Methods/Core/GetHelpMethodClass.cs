﻿

// Electrum-3.3.8


using System;

namespace JsonRPC.Request.Methods.Core
{
    /// <summary>
    /// for the python console
    /// </summary>
    class GetHelpMethodClass : AbstractMethodClass
    {
        public override string method => "help";
        public GetHelpMethodClass(Electrum_JSONRPC_Client client) : base(client)
        {

        }

        public override object execute()
        {
            return Client.Execute(method, options);
            // return deserialized object //Masoud
        }
    }
}
