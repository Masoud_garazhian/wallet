﻿

// Electrum-3.3.8


using System;
using System.ComponentModel.DataAnnotations;

namespace JsonRPC.Request.Methods.Wallet
{
    /// <summary>
    /// Remove a 'local' transaction from the wallet, and its dependent transactions.
    /// </summary>
    class RemoveLocalTransactionFromWalletAndDependentMethodClass : AbstractMethodClass // commands.py signature removelocaltx(self, txid):
    {
        public override string method => "removelocaltx";

        /// <summary>
        /// Transaction ID
        /// </summary>
        [Required]
        public string txid;

        public RemoveLocalTransactionFromWalletAndDependentMethodClass(Electrum_JSONRPC_Client client) : base(client)
        {

        }

        public override object execute()
        {
            if (string.IsNullOrWhiteSpace(txid))
                throw new ArgumentNullException("txid");

            options.Add("txid", txid);

            return Client.Execute(method, options);

            // return deserialized object //Masoud
        }
    }
}
