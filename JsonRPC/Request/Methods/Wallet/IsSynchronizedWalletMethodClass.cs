﻿

// Electrum-3.3.8


using System;

namespace JsonRPC.Request.Methods.Wallet
{
    /// <summary>
    /// Возврат состояния синхронизации Кошелька
    /// ~ ~ ~
    /// return wallet synchronization status
    /// </summary>
    class IsSynchronizedWalletMethodClass : AbstractMethodClass
    {
        public override string method => "is_synchronized";

        public IsSynchronizedWalletMethodClass(Electrum_JSONRPC_Client client) : base(client)
        {

        }

        public override object execute()
        {
            return Client.Execute(method, options);
            // return deserialized object //Masoud
        }
    }
}
