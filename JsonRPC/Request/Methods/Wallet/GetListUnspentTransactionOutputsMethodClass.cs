﻿using System;

namespace JsonRPC.Request.Methods.Wallet
{
    /// <summary>
    /// List unspent outputs. Returns the list of unspent transaction outputs in your wallet
    /// </summary>
    class GetListUnspentTransactionOutputsMethodClass : AbstractMethodClass
    {
        public override string method => "listunspent";

        public GetListUnspentTransactionOutputsMethodClass(Electrum_JSONRPC_Client client) : base(client)
        {

        }

        public override object execute()
        {
            return Client.Execute(method, options); 
            // return deserialized object //Masoud
        }
    }
}
