﻿using System;
using System.ComponentModel.DataAnnotations;

namespace JsonRPC.Request.Methods.Wallet
{
    /// <summary> 
    /// Deserialize a serialized transaction
    /// </summary>
    class DeserializeTransactionMethodClass : AbstractMethodClass // commands.py signature deserialize(self, tx):
    {
        public override string method => "deserialize";

        /// <summary>
        /// Serialized transaction (hexadecimal)
        /// </summary>
        [Required]
        public string tx;

        public DeserializeTransactionMethodClass(Electrum_JSONRPC_Client client) : base(client)
        {

        }

        public override object execute()
        {
            if (string.IsNullOrWhiteSpace(tx))
                throw new ArgumentNullException("tx");

            options.Add("tx", tx);
            return Client.Execute(method, options);
            // return deserialized object //Masoud
        }
    }
}
