﻿using System;
using System.ComponentModel.DataAnnotations;

namespace JsonRPC.Request.Methods.Wallet
{
    /// <summary> 
    /// Masoud
    /// Sign a transaction. The wallet keys will be used unless a private key is provided
    /// </summary>
    class SignTransactionMethodClass : AbstractMethodClass // commands.py signature signtransaction(self, tx, privkey=None, password=None):
    {
        public override string method => "signtransaction";

        /// <summary>
        /// Serialized transaction (hexadecimal)
        /// </summary>
        [Required]
        public string tx;

        /// <summary>
        /// Private key 
        /// </summary>
        public string privkey = null;
        public string password = null;

        public SignTransactionMethodClass(Electrum_JSONRPC_Client client) : base(client)
        {

        }

        public override object execute()
        {
            if (string.IsNullOrWhiteSpace(tx))
                throw new ArgumentNullException("tx");

            options.Add("tx", tx);

            if (!string.IsNullOrEmpty(privkey))
                options.Add("privkey", privkey);

            if (!string.IsNullOrEmpty(password))
                options.Add("password", password);

            return Client.Execute(method, options);
        }
    }
}
