﻿ 
namespace JsonRPC.Request.Methods.Payment
{
    /// <summary>
    /// Remove all payment requests.
    /// </summary>
    class RemoveAllPaymentRequestsMethodClass : AbstractMethodClass
    {
        public override string method => "clearrequests";

        public RemoveAllPaymentRequestsMethodClass(Electrum_JSONRPC_Client client) : base(client)
        {

        }

        public override object execute()
        {
            return Client.Execute(method, options);
            // Electrum just returns a NULL so we will never know if we succeeded 
        }
    }
}
