using System;
using System.Collections.Generic;
using System.Linq;
using models;
using models.Response;

namespace JsonRPC
{
    public class Electrum : IElectrum
    {
        private readonly Electrum_JSONRPC_Client electrum_JRPC;

        public Electrum()
        {
            this.electrum_JRPC = new Electrum_JSONRPC_Client("masoud", "garazhian", "http://127.0.0.1", 7777); //TODO: secure passwords
        }

        // public methods

        public Response<string> GetAddress()
        {
            try
            {
                return new Response<string>() { Result = electrum_JRPC.CreateNewAddress(), HttpStatus = System.Net.HttpStatusCode.OK };
            }
            catch (System.Exception ex)
            {
                return ReturnError<string>(ex, 10001);
            }
        } 

        public Response<List<string>> GetExpandedAddresses()
        {
            try
            {
                var addressList = electrum_JRPC.GetExListWalletAddresses().result.Select(a => a[0].ToString() + " " + a[1].ToString() + " " + a[2].ToString()).ToList();
                return new Response<List<string>>() { Result = addressList, HttpStatus = System.Net.HttpStatusCode.OK };
            }
            catch (System.Exception ex)
            {
                return ReturnError<List<string>>(ex, 10003);
            }
        }

        public Response<List<string>> GetAddresses()
        {
            try
            {
                var addressList = electrum_JRPC.GetListWalletAddresses().result.Select(a => a.ToString()).ToList();
                return new Response<List<string>>() { Result = addressList, HttpStatus = System.Net.HttpStatusCode.OK };
            }
            catch (System.Exception ex)
            {
                return ReturnError<List<string>>(ex, 10004);
            }
        } 

        public Response<string> CreateUnsignedTransaction(TransactionDTO transaction)
        {
            try
            {
                var utx = electrum_JRPC.CreateUnsignedTransaction(transaction);
                return new Response<string>() { Result = utx, HttpStatus = System.Net.HttpStatusCode.OK };
            }
            catch (System.Exception ex)
            {
                return ReturnError<string>(ex, 10009);
            }
        }

        public Response<string> SignTransaction(UnsignedTx uTransaction)
        {
            try
            {
                var utx = electrum_JRPC.SignTransaction(uTransaction.Tx);
                return new Response<string>() { Result = utx, HttpStatus = System.Net.HttpStatusCode.OK };
            }
            catch (System.Exception ex)
            {
                return ReturnError<string>(ex, 10010);
            }
        }

        //private methods

        private static Response<T> ReturnError<T>(Exception ex, int code, System.Net.HttpStatusCode httpStatus = System.Net.HttpStatusCode.InternalServerError)
        {
            return new Response<T>() { Result = default(T), Code = code, Exception = ex, HttpStatus = httpStatus };
        }
    }
}