﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Globalization;


public class StringParser
{
    public double GetDoubleFromString(string conv_data)
    {
        if (string.IsNullOrWhiteSpace(conv_data))
            return 0;

        int count_digt_separators = (conv_data.Split(new string[] { ".", "," }, StringSplitOptions.RemoveEmptyEntries).Length - 1);

        if (count_digt_separators > 1)
            return 0;

        if (count_digt_separators == 1)
            conv_data = "0" + conv_data + "0";

        conv_data = conv_data.Replace(".", CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator).Replace(",", CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator);

        double ret_val;
        if (!double.TryParse(conv_data, NumberStyles.AllowDecimalPoint, CultureInfo.CurrentCulture, out ret_val))
            return 0;

        return ret_val;
    }
}
