using System.Collections.Generic;
using models;
using models.Response;

namespace JsonRPC
{
    public interface IElectrum
    {
        Response<string> GetAddress();
        Response<List<string>> GetExpandedAddresses();
        Response<List<string>> GetAddresses();
        Response<string> CreateUnsignedTransaction(TransactionDTO transaction);
        Response<string> SignTransaction(UnsignedTx uTransaction);
    }
}