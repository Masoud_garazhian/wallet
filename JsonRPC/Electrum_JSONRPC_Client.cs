﻿using JsonRPC.Request;
using JsonRPC.Request.Methods;
using JsonRPC.Request.Methods.Address;
using JsonRPC.Request.Methods.Payment;
using JsonRPC.Request.Methods.Wallet;
using JsonRPC.Response.Model;
using System;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Text;

using Newtonsoft.Json;
using models;

namespace JsonRPC
{
    public class Electrum_JSONRPC_Client
    {
        #region properties

        public string host { get; protected set; } = "";

        public int port { get; protected set; } = 0;

        public string _method_ { get; protected set; }

        public string encoded_authorization { get; protected set; }

        public string rpcUsername { get; protected set; } = null;

        public string rpcPassword { get; protected set; } = null;

        public int request_id { get; protected set; } = 0;

        public string jsonrpc_response_raw { get; protected set; }

        public string CurrentHttpStatusCode { get; protected set; }
        public string CurrentStatusDescription { get; protected set; }

        #endregion

        public Electrum_JSONRPC_Client(string in_rpcUsername = null, string in_rpcPassword = null, string in_host = "http://127.0.0.1", int in_port = 7777, int in_id = 0)
        {
            host = in_host;
            port = in_port;
            rpcUsername = in_rpcUsername;
            rpcPassword = in_rpcPassword;
            request_id = in_id;
        }

        public string Execute(string method, NameValueCollection param_request)
        {
            json_request req = new json_request();
            //
            // add test net   
            // param_request.Add("testnet", "--testnet");

            req.id = request_id++;
            req.method = method;
            req.Request_params = param_request;


            string request_json = req.ToString();

            _method_ = method;

            jsonrpc_response_raw = null;

            var uriStr = host + ":" + port.ToString();

            #region http web request
            HttpWebRequest http = (HttpWebRequest)WebRequest.Create(new Uri(host + ":" + port.ToString()));

            if (string.IsNullOrEmpty(encoded_authorization))
                encoded_authorization = Convert.ToBase64String(Encoding.GetEncoding("ISO-8859-1").GetBytes(rpcUsername + ":" + rpcPassword));

            http.Headers.Add("Authorization", "Basic " + encoded_authorization);

            http.Accept = "application/json";
            http.ContentType = "application/json";
            http.Method = "POST";
            http.PreAuthenticate = true;

            ASCIIEncoding encoding = new ASCIIEncoding();
            byte[] bytes = encoding.GetBytes(request_json);

            Stream newStream = http.GetRequestStream();
            newStream.Write(bytes, 0, bytes.Length);
            newStream.Close();

            try
            {
                HttpWebResponse response = (HttpWebResponse)http.GetResponse();
                CurrentHttpStatusCode = response.StatusCode.ToString();
                CurrentStatusDescription = response.StatusDescription;

                if (response.StatusCode != HttpStatusCode.OK)
                {
                    //TODO: not ok log 
                }
                else
                {
                    Stream stream = response.GetResponseStream();
                    StreamReader sr = new StreamReader(stream);

                    jsonrpc_response_raw = sr.ReadToEnd().Replace(@"\n", "\n").Replace(@"\t", "\t").Replace(@"\r", "\r").Replace(@"\""", "\"").Replace(@"""{", "{").Replace(@"}""", "}"); //  //.Replace("\\\t", "\t").Replace("\\\n", "\n").Replace("\\\r", "\r");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("HTTP request error: " + e.Message);
            }
            #endregion

            return jsonrpc_response_raw;
        }
          

        public string CreateNewAddress()
        {
            CreateNewAddressMethodClass createNewAddressMethodClass = new CreateNewAddressMethodClass(this);
            SimpleStringResponseClass simpleStringResponseClass = (SimpleStringResponseClass)createNewAddressMethodClass.execute();

            return simpleStringResponseClass.result;// added result
        }

        public string LoadWallet() // TODO: complete
        {
            LoadWalletMethodClass loadWalletMethodClass = new LoadWalletMethodClass(this);
            SimpleStringResponseClass simpleStringResponseClass = (SimpleStringResponseClass)loadWalletMethodClass.execute();

            return simpleStringResponseClass.result;// added result
        } 
         

        public SimpleBoolResponseClass ValidateAddress(string address)
        {
            if (string.IsNullOrEmpty(address))
                address = "";

            ValidateAddressMethodClass validateAddressMethodClass = new ValidateAddressMethodClass(this) { address = address };
            SimpleBoolResponseClass simpleBoolResponseClass = (SimpleBoolResponseClass)validateAddressMethodClass.execute();

            return simpleBoolResponseClass;
        }
            
        public string CreateAndSignTransaction(TransactionDTO transaction)
        {
            var createTransaction = new CreateTransactionMethodClass(this)
            {
                amount = transaction.Amount.ToString(),
                destination = transaction.Destination,
                fee = transaction.Fee
            };
            var createTranactionRes = JsonConvert.DeserializeObject<SimpleStringResponseClass>(createTransaction.execute().ToString());
            return createTranactionRes.ToString();

        }
         

        public string CreateUnsignedTransaction(TransactionDTO transaction)
        {
            // unsigned => true 
            var createTransaction = new CreateTransactionMethodClass(this)
            {
                amount = transaction.Amount.ToString(),
                destination = transaction.Destination,
                fee = transaction.Fee,
                unsigned = true
            };
            var createTranactionRes = JsonConvert.DeserializeObject<SimpleStringResponseClass>(createTransaction.execute().ToString());
            return createTranactionRes.ToString();
        }
        public SimpleStringArrayResponseClass GetListWalletAddresses(bool? filter_receiving = null, bool? filter_change = null, bool? filter_frozen = null, bool? filter_unused = null, bool? filter_funded = null)
        {
            GetListWalletAddressesMethodClass electrum_list_addresses_method = new GetListWalletAddressesMethodClass(this)
            {
                receiving = filter_receiving,
                change = filter_change,
                frozen = filter_frozen,
                unused = filter_unused,
                funded = filter_funded,

                balance = null,
                labels = null
            };

            object list_addresses = electrum_list_addresses_method.execute();

            return (SimpleStringArrayResponseClass)list_addresses;
        }
        public SimpleStringArrayArrayResponseClass GetExListWalletAddresses(bool? filter_receiving = null, bool? filter_change = null, bool? filter_frozen = null, bool? filter_unused = null, bool? filter_funded = null)
        {
            GetListWalletAddressesMethodClass electrum_list_addresses_method = new GetListWalletAddressesMethodClass(this)
            {
                receiving = filter_receiving,
                change = filter_change,
                frozen = filter_frozen,
                unused = filter_unused,
                funded = filter_funded,

                balance = true,
                labels = true
            };

            object list_addresses = electrum_list_addresses_method.execute();

            return (SimpleStringArrayArrayResponseClass)list_addresses;
        }

        public string SignTransaction(string uTransaction)
        {
            // unsigned => true 
            var createTransaction = new SignTransactionMethodClass(this)
            {
                tx = uTransaction
            };
            var createTranactionRes = JsonConvert.DeserializeObject<SimpleStringResponseClass>(createTransaction.execute().ToString());
            return createTranactionRes.ToString();
        }
    }
}

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\ 
//? GetFeeRateMethodClass
//CreateTransactionMethodClass
//? GetListPaymentsRequestsMethodClass
//? AddTransactionToWalletMethodClass
//SignTransactionMethodClass
//BroadcastTransactionToNetworkMethodClass
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\ 
